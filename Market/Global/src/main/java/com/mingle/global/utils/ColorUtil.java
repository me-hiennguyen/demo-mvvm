package com.mingle.global.utils;

import android.graphics.Color;

public class ColorUtil {
    public static boolean isValid(String color){
        try{
            Color.parseColor(color);
            return true;
        }catch (Throwable throwable){
            return false;
        }
    }
}
