package com.mingle.market.ui.login.views;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.mingle.market.R;
import com.mingle.market.databinding.ActivitySplashBinding;
import com.mingle.market.ui.base.views.BaseActivity;
import com.mingle.market.ui.home.views.HomeActivity;
import com.mingle.market.ui.login.viewmodels.SplashViewModel;

public class SplashScreenActivity extends BaseActivity<ActivitySplashBinding, SplashViewModel> {

    @Override
    protected int layoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent.inject(this);
        new Handler().postDelayed(() -> navigator.startActivity(HomeActivity.class), 400);
    }
}
