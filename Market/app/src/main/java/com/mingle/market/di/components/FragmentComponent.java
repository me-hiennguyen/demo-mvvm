package com.mingle.market.di.components;

import com.mingle.market.di.modules.FragmentModule;
import com.mingle.market.di.modules.ViewModelModule;
import com.mingle.market.di.scopes.PerFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ActivityComponent.class, modules = {FragmentModule.class, ViewModelModule.class})
public interface FragmentComponent {
    // inject fragment here
}
