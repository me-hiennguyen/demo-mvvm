package com.mingle.market.di.modules;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.mingle.market.di.qualifiers.ChildFragmentManager;
import com.mingle.market.di.scopes.PerFragment;
import com.mingle.market.navigators.ChildFragmentNavigator;
import com.mingle.market.navigators.FragmentNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {

    private final Fragment fragment;

    public FragmentModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    @ChildFragmentManager
    FragmentManager provideChildFragmentManager() {
        return fragment.getChildFragmentManager();
    }

    @Provides
    @PerFragment
    FragmentNavigator provideFragmentNavigator() {
        return new ChildFragmentNavigator(fragment);
    }

}
