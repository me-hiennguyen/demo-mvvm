package com.mingle.market.ui.setting.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import com.mingle.market.R;
import com.mingle.market.databinding.ActivitySettingBinding;
import com.mingle.market.ui.base.views.BaseActivity;
import com.mingle.market.ui.setting.SettingViewModel;

public class SettingActivity extends BaseActivity<ActivitySettingBinding, SettingViewModel> {
    @Override
    protected int layoutId() {
        return R.layout.activity_setting;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(binding.layoutToolbar.toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
