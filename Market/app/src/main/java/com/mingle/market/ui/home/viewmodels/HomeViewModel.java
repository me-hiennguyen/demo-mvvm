package com.mingle.market.ui.home.viewmodels;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.mingle.market.models.Address;

import com.mingle.market.repositories.RealmRepository;
import com.mingle.market.repositories.SimpleRealmRepository;
import com.mingle.market.repositories.criterias.RealmCriteria;
import com.mingle.market.ui.base.viewmodels.BaseViewModel;

import io.realm.RealmConfiguration;

import java.util.List;

import javax.inject.Inject;

public class HomeViewModel extends BaseViewModel {
    @Inject
    RealmConfiguration realmConfiguration;

    private RealmRepository<Address> addressRepository;
    private MutableLiveData<List<Address>> observersAddresses = new MutableLiveData<>();

    public HomeViewModel(Application application) {
        super(application);
        getApplication().getAppComponent().inject(this);
        addressRepository = new SimpleRealmRepository<>(Address.class, realmConfiguration);

        RealmCriteria<Address> criteria = addressRepository.createCriteria();
        addressRepository.addListener(criteria.getRealmQuery().findAllAsync(), results -> {
            if (results.isLoaded() && results.isValid()) {
                observersAddresses.setValue(results);
            }
        });
    }

    @Override
    protected void onCleared() {
        if (addressRepository != null) {
            addressRepository.tearDown();
        }

        super.onCleared();
    }

    public MutableLiveData<List<Address>> getAddresses() {
        return observersAddresses;
    }

    public void addAddress(Address address) {
        addressRepository.insertOrUpdate(address);
    }
}
