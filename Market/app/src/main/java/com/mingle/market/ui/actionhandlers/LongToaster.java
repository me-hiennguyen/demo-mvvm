package com.mingle.market.ui.actionhandlers;

import android.content.Context;
import android.widget.Toast;

import com.mingle.market.ui.delegates.Toaster;

public class LongToaster implements Toaster {
    @Override
    public void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
