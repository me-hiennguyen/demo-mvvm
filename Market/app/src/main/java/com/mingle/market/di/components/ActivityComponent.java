package com.mingle.market.di.components;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.mingle.market.di.modules.ActivityModule;
import com.mingle.market.di.modules.ViewModelModule;
import com.mingle.market.di.qualifiers.ActivityContext;
import com.mingle.market.di.qualifiers.ActivityFragmentManager;
import com.mingle.market.di.scopes.PerActivity;
import com.mingle.market.navigators.Navigator;
import com.mingle.market.ui.login.views.SplashScreenActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = {ActivityModule.class, ViewModelModule.class})
public interface ActivityComponent {

    @ActivityContext
    Context activityContext();

    @ActivityFragmentManager
    FragmentManager defaultFragmentManager();

    Navigator navigator();

    void inject(SplashScreenActivity activity);
}
