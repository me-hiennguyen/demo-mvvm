package com.mingle.market.repositories;

import com.mingle.market.models.Model;
import com.mingle.market.repositories.criterias.Criteria;
import com.mingle.market.repositories.criterias.RealmCriteria;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.RealmResults;

import java.util.ArrayList;
import java.util.List;

public class SimpleRealmRepository<T extends RealmModel & Model> implements RealmRepository<T> {
    private Realm realm;
    private List<RealmResults<T>> realmResults = new ArrayList<>();
    private Class<T> clazz;

    public SimpleRealmRepository(Class<T> clazz, RealmConfiguration realmConfiguration) {
        this.clazz = clazz;
        realm = Realm.getInstance(realmConfiguration);
    }

    @Override
    public void insertOrUpdate(T entity) {
        realm.executeTransactionAsync(realm -> realm.copyToRealmOrUpdate(entity), Throwable::printStackTrace);
    }

    @Override
    public void delete(T entity) {
        throw new UnsupportedOperationException("RealmRepository not support this method");
    }

    @Override
    public void delete(Criteria criteria) {
        if (criteria instanceof RealmCriteria) {
            realm.executeTransactionAsync(realm ->
                    ((RealmCriteria) criteria).getRealmQuery().findAll().deleteAllFromRealm()
            );
        } else {
            throw new IllegalArgumentException("Must use RealmCriteria for this repository");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> searchFor(Criteria criteria) {
        if (criteria instanceof RealmCriteria) {
            return ((RealmCriteria<T>) criteria).getRealmQuery().findAll();
        }

        throw new IllegalArgumentException("Must use RealmCriteria for this repository");
    }

    @Override
    public List<T> getAll() {
        return realm.where(clazz).findAll();
    }

    @Override
    public T getById(int id) {
        return realm.where(clazz).findFirst();
    }

    @Override
    public void tearDown() {
        if (!realm.isClosed()) {
            for (RealmResults<T> item : realmResults) {
                if (item.isValid()) {
                    item.removeAllChangeListeners();
                }
            }

            realm.close();
        }
    }

    @Override
    public void addListener(RealmResults<T> realmResults, RealmChangeListener<RealmResults<T>> listener) {
        if (!this.realmResults.contains(realmResults)) {
            this.realmResults.add(realmResults);
        }

        realmResults.addChangeListener(listener);
    }

    @Override
    public RealmQuery<T> createRealmQuery() {
        return realm.where(clazz);
    }

    @Override
    public RealmResults<T> getRealmResults(RealmCriteria<T> criteria) {
        return criteria.getRealmQuery().findAll();
    }

    @Override
    public RealmCriteria<T> createCriteria() {
        return new RealmCriteria<>(createRealmQuery());
    }
}
