package com.mingle.market.ui.base.views;

import android.app.Activity;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mingle.market.GlobalApplication;
import com.mingle.market.di.components.ActivityComponent;
import com.mingle.market.di.components.DaggerActivityComponent;
import com.mingle.market.di.modules.ActivityModule;
import com.mingle.market.navigators.Navigator;
import com.mingle.market.ui.actionhandlers.SimpleToaster;
import com.mingle.market.ui.base.viewmodels.BaseViewModel;
import com.mingle.market.ui.delegates.Toaster;

import java.lang.reflect.ParameterizedType;

import javax.inject.Inject;


public abstract class BaseActivity<V extends ViewDataBinding, M extends BaseViewModel>
        extends AppCompatActivity implements LifecycleRegistryOwner, Toaster {
    private final LifecycleRegistry registry = new LifecycleRegistry(this);
    protected V binding;
    protected M viewModel;
    protected ActivityComponent activityComponent;

    @Inject
    protected Navigator navigator;

    @Override
    public LifecycleRegistry getLifecycle() {
        return registry;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindingView(layoutId());
        viewModel = ViewModelProviders.of(this).get(viewModelClass());
        activityComponent = DaggerActivityComponent.builder().appComponent(((GlobalApplication)getApplication()).getAppComponent()).activityModule(new ActivityModule(this)).build();
    }

    protected V bindingView(int layoutId) {
        return DataBindingUtil.setContentView(this, layoutId);
    }

    protected abstract int layoutId();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityComponent = null;
    }

    @SuppressWarnings("unchecked")
    private Class<M> viewModelClass() {
        // dirty hack to get generic type https://stackoverflow.com/a/1901275/719212
        return (Class<M>)
                ((ParameterizedType) getClass().getGenericSuperclass())
                        .getActualTypeArguments()[1];
    }

    protected Activity getActivity() {
        return this;
    }

    @Override
    public void showToast(Context context, String message) {
        getToaster().showToast(context, message);
    }

    protected Toaster getToaster() {
        return new SimpleToaster();
    }
}
