package com.mingle.market.ui.delegates;

import android.content.Context;

public interface Toaster {
    void showToast(Context context, String message);
}
