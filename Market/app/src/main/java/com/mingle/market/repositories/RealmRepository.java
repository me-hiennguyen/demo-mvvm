package com.mingle.market.repositories;


import com.mingle.market.models.Model;
import com.mingle.market.repositories.criterias.RealmCriteria;

import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public interface RealmRepository<T extends RealmModel & Model> extends Repository<T> {
    void addListener(RealmResults<T> realmResults, RealmChangeListener<RealmResults<T>> listener);

    RealmQuery<T> createRealmQuery();

    RealmResults<T> getRealmResults(RealmCriteria<T> criteria);

    RealmCriteria<T> createCriteria();
}
