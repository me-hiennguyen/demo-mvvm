package com.mingle.market.ui.home.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.mingle.market.R;
import com.mingle.market.databinding.ActivityHomeBinding;
import com.mingle.market.models.Address;
import com.mingle.market.ui.actionhandlers.LongToaster;
import com.mingle.market.ui.base.views.BaseActivity;
import com.mingle.market.ui.delegates.Toaster;
import com.mingle.market.ui.home.viewmodels.HomeViewModel;
import com.mingle.market.ui.setting.views.SettingActivity;

import java.util.Random;

public class HomeActivity extends BaseActivity<ActivityHomeBinding, HomeViewModel> {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(binding.layoutToolbar.toolbar);

        binding.fab.setOnClickListener(view -> {
            Address address = new Address();
            address.setId(new Random().nextInt(2222));
            address.setAddress1("Address " + new Random().nextInt(2222));
            address.setAddress2("City " + new Random().nextInt(2222));
            viewModel.addAddress(address);

        });

        getSupportFragmentManager().beginTransaction().replace(R.id.fl_content,
                new HomeFragment()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(getActivity(), SettingActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int layoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected Toaster getToaster() {
        return new LongToaster();
    }
}
