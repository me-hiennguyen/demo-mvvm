package com.mingle.market.binding;

import android.databinding.BindingAdapter;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.mingle.market.R;

@SuppressWarnings("unused")
public class ColorConverters {
    @BindingAdapter("backgroundColorRes")
    public static void setBackgroundColorRes(View view, Integer color) {
        if (view == null || color == null) {
            return;
        }

        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), color));
    }

    @BindingAdapter("textColorRes")
    public static void setTextColorRes(TextView view, Integer textColor) {
        if (view == null || textColor == null) {
            return;
        }

        if(textColor == R.color.colorPrimary){

        }

        view.setTextColor(ContextCompat.getColor(view.getContext(), textColor));
    }

    @BindingAdapter("hintColorRes")
    public static void setTextHintColorRes(TextView view, Integer hintColor) {
        if (view == null || hintColor == null) {
            return;
        }

        view.setHintTextColor(ContextCompat.getColor(view.getContext(), hintColor));
    }

    @BindingAdapter("linkColorRes")
    public static void setTextLinkColorRes(TextView view, Integer linkColor) {
        if (view == null || linkColor == null) {
            return;
        }

        view.setLinkTextColor(ContextCompat.getColor(view.getContext(), linkColor));
    }
}
