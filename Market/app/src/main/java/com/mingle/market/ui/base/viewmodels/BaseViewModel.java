package com.mingle.market.ui.base.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import com.mingle.market.GlobalApplication;


public class BaseViewModel extends AndroidViewModel {

    public BaseViewModel(Application application) {
        super(application);
    }

    @SuppressWarnings("unchecked")
    @Override
    public GlobalApplication getApplication() {
        return (GlobalApplication) super.getApplication();
    }
}
