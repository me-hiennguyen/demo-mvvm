package com.mingle.market.ui.home.views.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drextended.actionhandler.listener.ActionClickListener;
import com.drextended.rvdatabinding.adapter.BindingHolder;
import com.drextended.rvdatabinding.delegate.ActionAdapterDelegate;
import com.mingle.market.R;
import com.mingle.market.databinding.ItemAdBinding;
import com.mingle.market.models.Ad;
import com.mingle.market.models.Model;

import java.util.List;

public class AdDelegate extends ActionAdapterDelegate<Model, ItemAdBinding> {
    public AdDelegate(ActionClickListener actionHandler) {
        super(actionHandler);
    }

    @Override
    public boolean isForViewType(@NonNull List<Model> items, int position) {
        return items.get(position) instanceof Ad;
    }

    @NonNull
    @Override
    public BindingHolder<ItemAdBinding> onCreateViewHolder(ViewGroup parent) {
        return BindingHolder.newInstance(R.layout.item_ad, LayoutInflater.from(parent.getContext()), parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull List<Model> items, int position, @NonNull BindingHolder<ItemAdBinding> holder) {
        final Ad ad = (Ad) items.get(position);
        holder.getBinding().setAd(ad);
        holder.getBinding().setActionHandler(getActionHandler());
    }
}