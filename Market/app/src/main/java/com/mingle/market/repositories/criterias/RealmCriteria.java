package com.mingle.market.repositories.criterias;

import io.realm.RealmModel;
import io.realm.RealmQuery;

public class RealmCriteria<T extends RealmModel> extends Criteria {
    private RealmQuery<T> realmQuery;

    public RealmCriteria(RealmQuery<T> realmQuery) {
        this.realmQuery = realmQuery;
    }

    public RealmQuery<T> getRealmQuery() {
        return realmQuery;
    }

    public void setRealmQuery(RealmQuery<T> realmQuery) {
        this.realmQuery = realmQuery;
    }
}
