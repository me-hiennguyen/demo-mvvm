package com.mingle.market;

import android.app.Application;

import com.mingle.market.di.components.AppComponent;
import com.mingle.market.di.components.DaggerAppComponent;
import com.mingle.market.di.modules.AppModule;

import io.realm.Realm;

public class GlobalApplication extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        appComponent =  DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
