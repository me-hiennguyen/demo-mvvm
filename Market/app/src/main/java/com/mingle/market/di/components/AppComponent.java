package com.mingle.market.di.components;

import android.content.Context;
import android.content.res.Resources;

import com.mingle.market.di.modules.AppModule;
import com.mingle.market.di.modules.ServiceModule;
import com.mingle.market.di.qualifiers.AppContext;
import com.mingle.market.di.scopes.PerApplication;
import com.mingle.market.ui.home.viewmodels.HomeViewModel;

import dagger.Component;

@PerApplication
@Component(modules = {AppModule.class, ServiceModule.class})
public interface AppComponent {
    @AppContext
    Context appContext();

    Resources resources();

    void inject(HomeViewModel viewModel);
}
