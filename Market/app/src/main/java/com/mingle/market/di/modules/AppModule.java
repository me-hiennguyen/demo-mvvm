package com.mingle.market.di.modules;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.mingle.market.di.qualifiers.AppContext;
import com.mingle.market.di.scopes.PerApplication;
import com.mingle.market.realm.RealmModules;
import com.mingle.market.utils.Config;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmConfiguration;

@Module
public class AppModule {
    private final Application app;

    public AppModule(Application app) {
        this.app = app;
    }

    @Provides
    @PerApplication
    @AppContext
    Context provideAppContext() {
        return app;
    }

    @Provides
    @PerApplication
    Resources provideResources() {
        return app.getResources();
    }

    @Provides
    @PerApplication
    RealmConfiguration provideConfiguration() {
        return new RealmConfiguration.Builder()
                .name(Config.DATABASE_NAME)
                .schemaVersion(Config.DATABASE_VERSION)
                .modules(new RealmModules())
                .deleteRealmIfMigrationNeeded()
                .build();
    }
}
