package com.mingle.market.ui.home.views;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import com.android.databinding.library.baseAdapters.BR;
import com.drextended.actionhandler.ActionHandler;
import com.drextended.actionhandler.action.BaseAction;
import com.drextended.actionhandler.listener.ActionClickListener;
import com.drextended.rvdatabinding.ListConfig;
import com.drextended.rvdatabinding.adapter.BindableAdapter;
import com.drextended.rvdatabinding.delegate.ModelActionItemDelegate;
import com.mingle.market.R;
import com.mingle.market.databinding.FragmentHomeBinding;
import com.mingle.market.models.ActionType;
import com.mingle.market.models.Ad;
import com.mingle.market.models.Address;
import com.mingle.market.models.Model;
import com.mingle.market.ui.base.views.BaseFragment;
import com.mingle.market.ui.home.viewmodels.HomeViewModel;
public class HomeFragment extends BaseFragment<FragmentHomeBinding> {
    private BindableAdapter<List<Model>> adapter;

    @Override
    protected int layoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        HomeViewModel viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        binding.setViewModel(viewModel);
        final ActionClickListener actionHandler = new ActionHandler.Builder()
                .addAction(ActionType.OPEN_URL, new BaseAction<Ad>() {
                    @Override
                    public boolean isModelAccepted(Object model) {
                        return model instanceof Ad;
                    }

                    @Override
                    public void onFireAction(Context context, @Nullable View view, @Nullable String actionType, @Nullable Ad model) {
                        Toast.makeText(context, String.format(Locale.US, "Click on ad %s with url %s", model.getName(), model.getBanner()),
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .build();

        //noinspection unchecked
        adapter = new BindableAdapter<>(
//                new AddressDelegate(actionHandler),
//                new AdDelegate(actionHandler),
                new ModelActionItemDelegate<>(actionHandler, Address.class, R.layout.item_address, BR.address),
                new ModelActionItemDelegate<>(actionHandler, Ad.class, R.layout.item_ad, BR.ad)
        );

        ListConfig listConfig = new ListConfig.Builder(adapter)
                .setDefaultDividerEnabled(true)
                .build(getContext());
        listConfig.applyConfig(getContext(), binding.rvTodo);

        viewModel.getAddresses().observe(this, todoList -> {
            List<Model> items = new ArrayList<>();
            if (todoList != null) {
                for (int i = 0; i < todoList.size(); i++) {
                    items.add(todoList.get(i));
                    Ad ad = new Ad();
                    ad.setName("ad " + i);
                    ad.setBanner("http://techli.com/wp-content/uploads/2012/05/advertising-banner.jpg");
                    items.add(ad);
                }
            }

            adapter.setItems(items);
            adapter.notifyDataSetChanged();
        });

    }
}
