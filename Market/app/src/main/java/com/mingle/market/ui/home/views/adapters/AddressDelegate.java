package com.mingle.market.ui.home.views.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drextended.actionhandler.listener.ActionClickListener;
import com.drextended.rvdatabinding.adapter.BindingHolder;

import com.drextended.rvdatabinding.delegate.ActionAdapterDelegate;
import com.mingle.market.R;
import com.mingle.market.databinding.ItemAddressBinding;
import com.mingle.market.models.Address;
import com.mingle.market.models.Model;

import java.util.List;


public class AddressDelegate extends ActionAdapterDelegate<Model, ItemAddressBinding> {
    public AddressDelegate(ActionClickListener actionHandler) {
        super(actionHandler);
    }

    @Override
    public boolean isForViewType(@NonNull List<Model> items, int position) {
        return items.get(position) instanceof Address;
    }

    @NonNull
    @Override
    public BindingHolder<ItemAddressBinding> onCreateViewHolder(ViewGroup parent) {
        return BindingHolder.newInstance(R.layout.item_address, LayoutInflater.from(parent.getContext()), parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull List<Model> items, int position, @NonNull BindingHolder<ItemAddressBinding> holder) {
        final Address address = (Address) items.get(position);
        holder.getBinding().setAddress(address);
        holder.getBinding().setActionHandler(getActionHandler());
    }
}
