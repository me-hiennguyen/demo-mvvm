package com.mingle.market.repositories;


import com.mingle.market.repositories.criterias.Criteria;

import java.util.List;

public interface Repository<T> {
    void insertOrUpdate(T entity);

    void delete(T entity);

    void delete(Criteria criteria);

    List<T> searchFor(Criteria criteria);

    List<T> getAll();

    T getById(int id);

    void tearDown();
}
