package com.mingle.market.di.modules;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.mingle.market.di.qualifiers.ActivityContext;
import com.mingle.market.di.qualifiers.ActivityFragmentManager;
import com.mingle.market.di.scopes.PerActivity;
import com.mingle.market.navigators.ActivityNavigator;
import com.mingle.market.navigators.Navigator;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private final AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    @ActivityContext
    Context provideActivityContext() {
        return activity;
    }

    @Provides
    @PerActivity
    @ActivityFragmentManager
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @Provides
    @PerActivity
    Navigator provideNavigator() {
        return new ActivityNavigator(activity);
    }
}
